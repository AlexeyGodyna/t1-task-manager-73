package ru.godyna.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.godyna.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("api/projects")
public interface IProjectRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/deleteAll")
    void deleteAll();

}
