package ru.t1.godyna.tm.api.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

}
