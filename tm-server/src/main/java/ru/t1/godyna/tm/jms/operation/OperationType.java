package ru.t1.godyna.tm.jms.operation;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
