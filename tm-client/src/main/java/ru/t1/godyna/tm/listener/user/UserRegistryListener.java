package ru.t1.godyna.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.user.UserRegistryRequest;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.event.ConsoleEvent;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    private final static String NAME = "user-registry";

    @NotNull
    private final static String DESCRIPTION = "registry user.";

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken(), login, password, email);
        userEndpoint.registryUser(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
