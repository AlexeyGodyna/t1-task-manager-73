package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

}
