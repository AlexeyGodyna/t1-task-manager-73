package ru.t1.godyna.tm.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class ConsoleEvent {

    @NotNull
    private final String name;

}
