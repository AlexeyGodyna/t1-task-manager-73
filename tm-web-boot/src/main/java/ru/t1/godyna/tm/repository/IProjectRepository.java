package ru.t1.godyna.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    List<Project> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @Nullable
    Project findByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

}
