package ru.t1.godyna.tm.exceprion.user;

public class UsernameFoundException extends AbstractUserException {

    public UsernameFoundException() {
        super("Error! Permission is incorrect...");
    }

}
