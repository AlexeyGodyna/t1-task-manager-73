package ru.t1.godyna.tm.exceprion.user;

import org.jetbrains.annotations.NotNull;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login alredy exists...");
    }

    public ExistsLoginException(@NotNull String login) {
        super("Error! Login '" + login + "' alredy exists...");
    }

}
