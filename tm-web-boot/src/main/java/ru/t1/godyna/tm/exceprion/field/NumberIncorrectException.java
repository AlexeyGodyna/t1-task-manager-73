package ru.t1.godyna.tm.exceprion.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbsractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("Error! This value '" + value + "' is incorrect...");
    }

}
