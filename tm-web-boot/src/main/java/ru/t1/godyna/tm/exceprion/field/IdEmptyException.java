package ru.t1.godyna.tm.exceprion.field;

public final class IdEmptyException extends AbsractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
