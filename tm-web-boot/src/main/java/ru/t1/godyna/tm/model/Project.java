package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.godyna.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public class Project {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public Project(String name) {
        this.name = name;
    }

}
