package ru.t1.godyna.tm.exceprion.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.exceprion.AbstractException;

@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    public AbstractUserException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
