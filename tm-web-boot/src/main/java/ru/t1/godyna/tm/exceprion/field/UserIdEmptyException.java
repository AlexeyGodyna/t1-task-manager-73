package ru.t1.godyna.tm.exceprion.field;

public class UserIdEmptyException extends AbsractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id not found...");
    }

}
